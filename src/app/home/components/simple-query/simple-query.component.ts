import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { CompositeQueryElement, NgxQueryBuilderComponent, QueryProperty } from '@universis/ngx-query-builder';
import { remove } from 'lodash';
@Component({
  selector: 'app-simple-query',
  templateUrl: './simple-query.component.html'
})
export class SimpleQueryComponent implements OnInit {

  public queryParams: any;

  @ViewChild('queryBuilder') queryBuilder: NgxQueryBuilderComponent;

  public readonly query: CompositeQueryElement = {
    type: 'LogicalExpression',
        operator: '$or',
        elements: [
          {
            type: 'BinaryExpression',
            operator: '$eq',
            left: '$graduationDate',
            right: '2020-12-22'
          },
          {
            type: 'BinaryExpression',
            operator: '$ge',
            left: '$inscriptionYear',
            right: {
              "raw": 2018,
              "label": "2018-2019"
            }
          }
        ]
  };

  constructor(private context: AngularDataContext) { }

  ngOnInit(): void {
  }

  public onLoadingProperties(properties: QueryProperty[]) {
    // remove specific attributes
    remove(properties, (item: QueryProperty) => {
      return [
        'attachments',
        'customField1',
        'customField2',
        'customField3',
        'customField4',
        'customField5',
      ].includes(item.label)
    });
    // or use QueryProperty.hidden
    // (properties.find((item) => item.label === 'attachments'))!.hidden = true;
    const studyProgramSpecialty = properties.find((property) => {
      return property.value === '$studyProgramSpecialty'
    });
    if (studyProgramSpecialty) {
      studyProgramSpecialty.source = '/StudyProgramSpecialties?$top={{limit}}&$skip={{skip}}&$select=id as raw,name as label';
    }
  }

  getQueryParams() {
    this.queryParams = this.queryBuilder.queryParams;
  }

}
