import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { SimpleQueryComponent } from './components/simple-query/simple-query.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'index'
  },
  {
    path: 'index',
    component: HomeComponent,
    data: {
      title: 'Home'
    }
  },
  {
    path: 'simple-query',
    component: SimpleQueryComponent,
    data: {
      title: 'Home'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
