import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';
import { SimpleQueryComponent } from './components/simple-query/simple-query.component';
import { QueryBuilderModule } from 'projects/ngx-query-builder/src/public-api';
import { MostModule } from '@themost/angular';

@NgModule({
  declarations: [
    HomeComponent,
    SimpleQueryComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MostModule,
    HomeRoutingModule,
    QueryBuilderModule
  ]
})
export class HomeModule { }
