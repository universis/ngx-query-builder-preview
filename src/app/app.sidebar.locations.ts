// tslint:disable: trailing-comma
export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Home',
    key: 'Sidebar.Dashboard',
    url: '/home/index',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'Simple Query',
    key: 'Sidebar.SimpleQuery',
    url: '/home/simple-query',
    icon: 'fa fa-archive',
    index: 0
  }
];
